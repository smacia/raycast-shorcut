import { Color, Icon, List } from "@raycast/api";
import { Story, useStories } from "./useStories";
import { StoryAction } from "./item";
const tintedIcon = { source: Icon.Bubble, tintColor: Color.Red };

export default function Command() {
  const [items, isLoading, error] = useStories();

  return (
    <List isLoading={(!items && !error) || isLoading} navigationTitle="Shortcut Stories">
      {items
        ?.filter((item: Story) => item.completed === false)
        .map((item) => (
          <List.Item
            title={item.name}
            key={item.id}
            subtitle={`[SC-${String(item.id)}]`}
            actions={<StoryAction item={item} />}
            accessories={[
              {
                date: new Date(Date.parse(item.updated_at)),
                icon: Icon.Clock,
              },
              {
                text: item.workflow,
                icon: tintedIcon,
              },
            ]}
          />
        ))}
    </List>
  );
}
