import axios from "axios";
import { useEffect, useState } from "react";
import { getPreferenceValues } from "@raycast/api";

export interface Story {
  app_url: string;
  name: string;
  id: number;
  updated_at: string;
  workflow_id: string;
  workflow: string;
  completed: boolean;
}
interface State {
  isLoading: boolean;
  items: Story[];
  error: Error | null;
}

interface Preferences {
  token: string;
}

export const useStories = (): [Story[], boolean, Error | null] => {
  const [state, setState] = useState<State>({
    items: [],
    isLoading: true,
    error: null,
  });

  useEffect(() => {
    async function fetchStories() {
      setState((previous) => ({ ...previous, isLoading: true }));
      try {
        const preferences: Preferences = getPreferenceValues();
        axios.defaults.headers.common["Shortcut-Token"] = preferences.token;

        const user = await axios.get("https://api.app.shortcut.com/api/v3/member");
        const workflows = await axios.get("https://api.app.shortcut.com/api/v3/workflows");
        const dictionary = Object.assign({}, ...workflows.data.map((x: any) => ({ [x.id]: x.name })));
        const feed = await axios.get("https://api.app.shortcut.com/api/v3/search/stories", {
          params: { query: `owner:${user.data.mention_name}` },
        });
        const feedParsed = feed.data.data.map((item: Story) => ({
          app_url: item.app_url,
          name: item.name,
          id: item.id,
          workflow_id: String(item.workflow_id),
          workflow: dictionary[item.workflow_id],
          updated_at: item.updated_at,
          completed: item.completed,
        }));
        console.log(feedParsed);

        setState((previous) => ({
          ...previous,
          items: feedParsed,
          isLoading: false,
        }));
      } catch (error) {
        console.log(error);
        setState((previous) => ({
          ...previous,
          error: error instanceof Error ? error : new Error("Something went wrong"),
          isLoading: false,
          items: [],
        }));
      }
    }

    fetchStories();
  }, []);

  return [state.items, state.isLoading, state.error];
};
