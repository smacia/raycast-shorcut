import { ActionPanel, Action } from "@raycast/api";
import { Story } from "./useStories";

export function StoryAction(props: { item: Story }) {
  return (
    <ActionPanel title={props.item.name}>
      <ActionPanel.Section>
        {props.item.app_url && <Action.OpenInBrowser url={props.item.app_url} />}
      </ActionPanel.Section>
      <ActionPanel.Section>
        {props.item.app_url && (
          <Action.CopyToClipboard
            content={props.item.app_url}
            title="Copy Link"
            shortcut={{ modifiers: ["cmd"], key: "." }}
          />
        )}
      </ActionPanel.Section>
    </ActionPanel>
  );
}
